require 'rails_helper'

RSpec.describe 'Newsletter subscription', type: :feature do
  let(:page) { Pages::Home::Index.new }
  let!(:seed) { 3.times { create(:newsletter) } }

  context 'sending subscription', js: true do
    before do
      @subscription = build(:subscription)
      page.load
    end
    scenario 'will be directed to the Landlords home' do
      page.newsletter.set Newsletter.first.id
      page.name.set @subscription.name
      page.email.set @subscription.email
      page.submit.click
      page.wait_until_flash_success_visible
      expect(page.flash_success.text).to match /^*.?Subscription confirmed!/
      expect(page).to have_current_path(root_path)
    end
  end

end
