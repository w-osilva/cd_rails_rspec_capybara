FactoryBot.define do
  factory :subscription do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    association :newsletter
  end
end
