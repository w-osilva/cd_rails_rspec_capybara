require 'site_prism'

module SitePrism
  module Helpers
    def input(name)
      element name, "[name$='[#{name}]']"
    end

    alias_method :select, :input
    alias_method :checkbox, :input
    alias_method :radio, :input
    alias_method :textarea, :input

    def association(name)
      element name, "[name$='[#{name}_id]']"
    end

    def bs_switch(name)
      element name, "[class$='#{name}']"
    end

    def submit(identifier = '#submit')
      element :submit, identifier
    end

    def button(name)
      element name, "button[name='#{name}']"
    end

    def link(name)
      element name, "a[name='#{name}']"
    end
  end

  class Page
    extend Helpers

    element :flash_success, '.alert.alert-success'
    element :flash_error, '.alert.alert-danger'
    element :flash_warning, '.alert.alert-warning'
    element :modal, '.modal > .modal-dialog'

    def has_success_toastr?
      has_toast_success?
    end

    def has_error_toastr?
      has_toast_error?
    end

    def has_warning_toastr?
      has_toast_warning?
    end
  end
end
