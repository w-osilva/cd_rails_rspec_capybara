require 'capybara/rspec'

Capybara.register_driver :chrome do |app|
  caps = Selenium::WebDriver::Remote::Capabilities.chrome(chromeOptions: {
      args: %[window-size=1440,900 headless disable-gpu enable-features=NetworkService,NetworkServiceInProcess]
  })
  Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: caps)
end

Capybara.default_driver = :chrome
Capybara.javascript_driver = :chrome
Capybara.default_max_wait_time = 5