require 'webmock/rspec'

allowed_sites = %w( 127.0.0.1 )

WebMock.disable_net_connect!(:allow_localhost => true, allow: allowed_sites)
