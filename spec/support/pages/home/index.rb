module Pages
  module Home
    class Index < SitePrism::Page
      set_url '/'

      association :newsletter
      input :name
      input :email
      submit 'button[type=submit]'
    end
  end
end