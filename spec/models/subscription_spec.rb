require 'rails_helper'

RSpec.describe Subscription, type: :model do
  describe 'associations' do
    it { should belong_to(:newsletter) }
  end

  describe 'attributes' do
    subject { build(:subscription).attributes.symbolize_keys }
    it { is_expected.to include(:id, :name, :email, :newsletter_id) }
  end

  describe 'validations' do
    subject { build(:subscription) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:newsletter) }
  end
end