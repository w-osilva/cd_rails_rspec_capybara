require 'rails_helper'

RSpec.describe Newsletter, type: :model do
  describe 'associations' do
    it { should have_many(:subscriptions) }
  end

  describe 'attributes' do
    subject { build(:newsletter).attributes.symbolize_keys }
    it { is_expected.to include(:id, :subject) }
  end

  describe 'validations' do
    subject { build(:newsletter) }
    it { should validate_presence_of(:subject) }
  end
end