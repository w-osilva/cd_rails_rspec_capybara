class HomeController < ApplicationController
  def index
    @subscription = Subscription.new``
  end

  def create_subscription
    @subscription = Subscription.new(subscription_params)

    if @subscription.save
      flash[:notice] = "Subscription confirmed!"
      redirect_to root_path
    else
      flash[:error] = "Subscription failed."
      render :index
    end
  end

  private
  def subscription_params
    params.require(:subscription).permit(:name, :email, :newsletter_id)
  end
end
