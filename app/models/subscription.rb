class Subscription < ApplicationRecord
  belongs_to :newsletter
  validates :name, presence: true
  validates :email, presence: true
  validates :newsletter, presence: true
end
